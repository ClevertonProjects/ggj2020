﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public class MenuController : MonoBehaviour
{
    public GameObject fader;

    [SerializeField] private Button[] optionsButtons;
    [SerializeField] private Animator[] optionsAnims;
    [SerializeField] private Animator creditsAnim;
    private int selectedOptionIndex;
    [SerializeField] private float inputDelay;
    private float inputCountdown;

    private bool isOnCredits;

    private HandyInputs inputActions;
    private Vector2 leftStickInput;

    private const string SelectAnimTag = "IsSelected";
    private const string ShowCreditsTag = "ShowCredits";
    private const string HideCreditsTag = "Hide";

    [Header("Audio")]
    [SerializeField] private AudioSource selectSfx;

    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }

    private void Awake()
    {
        inputActions = new HandyInputs();
        InputUser _user = InputUser.PerformPairingWithDevice(Gamepad.all[0]);
        _user.AssociateActionsWithUser(inputActions);
        _user.ActivateControlScheme(inputActions.controlSchemes[0]);
        inputActions.gameplay.@throw.performed += ctx => ProcessSubmit(ctx);
    }

    // Start is called before the first frame update
    void Start  ()
    {
        GameObject spawnedFader =  Instantiate(fader);
        SceneFader sceneFader = spawnedFader.GetComponent<SceneFader>();
        sceneFader.FadeScene(FadeState.FadeOut);

        optionsAnims[selectedOptionIndex].SetBool(SelectAnimTag, true);
        optionsButtons[selectedOptionIndex].interactable = true;
    }

    private void Update()
    {
        GetInputs();
    }

    private void ProcessSubmit (InputAction.CallbackContext context)
    {
        if (selectedOptionIndex == 0)
        {
            TriggerSceneLoading();
        }
        else if (selectedOptionIndex == 1)
        {
            if (!isOnCredits)
            {
                isOnCredits = true;
                creditsAnim.SetTrigger(ShowCreditsTag);
            }
            else
            {
                isOnCredits = false;
                creditsAnim.SetTrigger(HideCreditsTag);
            }
        }
        else
        {
            Application.Quit();
        }
    }

    private void GetInputs ()
    {
        if (!isOnCredits)
        {
            if (inputCountdown > 0f)
            {
                inputCountdown -= Time.deltaTime;
            }
            else
            {
                leftStickInput = inputActions.gameplay.moveLeft.ReadValue<Vector2>();
                int axisDir = Mathf.RoundToInt(leftStickInput.y);

                int newIndex = Mathf.Clamp(selectedOptionIndex - axisDir, 0, optionsButtons.Length - 1);

                if (newIndex != selectedOptionIndex)
                {
                    optionsAnims[selectedOptionIndex].SetBool(SelectAnimTag, false);
                    optionsButtons[selectedOptionIndex].interactable = false;

                    optionsAnims[newIndex].SetBool(SelectAnimTag, true);
                    optionsButtons[newIndex].interactable = true;

                    selectedOptionIndex = newIndex;
                    inputCountdown = inputDelay;

                    selectSfx.Play();
                }
            }
        }        
    }

    private void ChangeScene ()
    {
        SceneManager.LoadScene(SceneIndex.Main);
    }

    public void TriggerSceneLoading ()
    {
        SceneFader fader = GameObject.FindGameObjectWithTag(Tags.Fader).GetComponent<SceneFader>();
        fader.FadeScene(FadeState.FadeIn, ChangeScene);
    }
}
