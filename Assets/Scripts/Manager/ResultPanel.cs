﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPanel : MonoBehaviour
{
    [SerializeField] private Sprite[] victoryStances;    
    [SerializeField] private Image imgVictoryLogo;
    [SerializeField] private RectTransform waveRect;
    [SerializeField] private RectTransform fadePanel;

    public void ShowResult (int winnerIndex)
    {   
        if (winnerIndex == 1)
        {
            imgVictoryLogo.sprite = victoryStances[winnerIndex];
            RectTransform victoryRect = imgVictoryLogo.rectTransform;
            waveRect.anchoredPosition = new Vector2(-waveRect.anchoredPosition.x, waveRect.anchoredPosition.y);
            fadePanel.anchoredPosition = new Vector2(-fadePanel.anchoredPosition.x, fadePanel.anchoredPosition.y);
            victoryRect.anchoredPosition = new Vector2(-victoryRect.anchoredPosition.x, victoryRect.anchoredPosition.y);
        }

        waveRect.GetComponent<Animator>().enabled = true;
        gameObject.SetActive(true);
    }

    private void EndLevel ()
    {
        GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>().TriggerSceneLoading();
    }

    private IEnumerator EndLevelRoutine ()
    {
        yield return new WaitForSeconds(4f);
        GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>().TriggerSceneLoading();
    }

    public void OnResultEnd ()
    {
        StartCoroutine(EndLevelRoutine());
    }
}
