﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{    
    [SerializeField] private LevelSettings levelSettings; 

    [Space]

    public static float leftWaterFill;
    public static float rightWaterFill;
    public static System.Action<float, float> OnGameOverAction;

    public GameObject fader;
    [SerializeField] private HazardsArea leftHazardArea;
    [SerializeField] private HazardsArea rightHazardArea;

    [SerializeField] private ResultPanel result;

    [SerializeField] private AudioSource getReadySfx;
    [SerializeField] private ParticleSystem confettiPrefab;    

    private void Start()
    {
        leftWaterFill = 0;
        rightWaterFill = 0;

        GameObject spawnedFader = Instantiate(fader);
        SceneFader sceneFader = spawnedFader.GetComponent<SceneFader>();
        sceneFader.FadeScene(FadeState.FadeOut, TriggerLevelBegin);

        //characterCreator.SpawnCharacters();

        OnGameOverAction += OnGameOver;
    }

    private void OnDestroy()
    {
        OnGameOverAction = null;
    }

    private void TriggerLevelBegin ()
    {      
        StartCoroutine(LevelStart());
        StartCoroutine(EndGameCondition());
    }

    private IEnumerator LevelStart ()
    {
        yield return new WaitForSeconds(1);
        getReadySfx.Play();
        leftHazardArea.StartWave();
        rightHazardArea.StartWave();
    }

    private void ChangeScene()
    {
        SceneManager.LoadScene(SceneIndex.Menu);
    }

    public void TriggerSceneLoading()
    {
        SceneFader fader = GameObject.FindGameObjectWithTag(Tags.Fader).GetComponent<SceneFader>();
        fader.FadeScene(FadeState.FadeIn, ChangeScene);
    }

    private IEnumerator EndGameCondition()
    {
        yield return new WaitUntil(() => leftWaterFill >= levelSettings.MaxWaterFill || rightWaterFill >= levelSettings.MaxWaterFill);
        OnGameOverAction.Invoke(leftWaterFill, rightWaterFill);                
    }

    private void OnGameOver(float p1Fill, float p2Fill)
    {       
        int winnerIndex = p1Fill > p2Fill ? 1 : 0;
        result.ShowResult(winnerIndex);

        Vector3 confettiPos = (winnerIndex == 0 ? leftHazardArea : rightHazardArea).transform.position + Vector3.up * 4;
        Instantiate(confettiPrefab, confettiPos, confettiPrefab.transform.rotation);
    }
}
