// GENERATED AUTOMATICALLY FROM 'Assets/ScriptableObjects/HandyInputs.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @HandyInputs : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @HandyInputs()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""HandyInputs"",
    ""maps"": [
        {
            ""name"": ""gameplay"",
            ""id"": ""32953a2e-4226-4b4d-b83e-1e3d9e873f45"",
            ""actions"": [
                {
                    ""name"": ""moveLeft"",
                    ""type"": ""Value"",
                    ""id"": ""e5606402-9684-4b1a-9297-cab43569542c"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""moveRight"",
                    ""type"": ""Value"",
                    ""id"": ""6aed9444-d255-4fd2-992f-9c01c3d0a6af"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""grabLeft"",
                    ""type"": ""Button"",
                    ""id"": ""2ab163c8-b1f9-4268-a7a0-6a54a8a42863"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""releaseLeft"",
                    ""type"": ""Button"",
                    ""id"": ""89dba103-7fa9-4ad5-9d1b-137ff77568e8"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""grabRight"",
                    ""type"": ""Button"",
                    ""id"": ""d61310c9-e206-45cd-8304-0b4b96f3cfb3"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""releaseRight"",
                    ""type"": ""Button"",
                    ""id"": ""fb46e9d0-def5-433e-af94-b7d135d748c1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""throw"",
                    ""type"": ""Button"",
                    ""id"": ""c80d1d74-1e42-468a-aa5a-00e82988e5f6"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap(duration=0.3)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""c2fc2072-7f6c-4b9f-8d89-9c1d91cb07ea"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4e177aad-7aee-492a-96dc-337ff82eda0f"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d3a8a74-596d-4185-8e29-0c443f2b05fd"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""grabLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7e3d87d9-51fd-4da0-b6ad-768d3df70a42"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""grabLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3b34d018-2634-4002-ae13-67f7735af8fa"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""grabRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7c9bc6a0-519d-46ff-b9d9-dd90a3b455ed"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""grabRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7f67674a-80cc-4e22-be5e-8d1b3a744029"",
                    ""path"": ""<DualShockGamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""throw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f9aaf10b-4c93-47c5-a951-f0ff295d66d2"",
                    ""path"": ""<XInputController>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""throw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""70d6cfce-2fad-4ab5-b0c6-8a7668930943"",
                    ""path"": ""<DualShockGamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""throw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""527be6eb-8c77-4024-8269-6ec31243bef6"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""throw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""471b912f-1f7f-4052-b0ed-5271fc2822e6"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""releaseLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""942bffab-f666-4407-959b-e0d9ed153b35"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""releaseLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ca498130-0897-4005-9c6f-7f94a56d8c9b"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""releaseRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f9e6961c-5a3b-4867-9cb1-efe1ee67b1ab"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""releaseRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""GamePad"",
            ""bindingGroup"": ""GamePad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // gameplay
        m_gameplay = asset.FindActionMap("gameplay", throwIfNotFound: true);
        m_gameplay_moveLeft = m_gameplay.FindAction("moveLeft", throwIfNotFound: true);
        m_gameplay_moveRight = m_gameplay.FindAction("moveRight", throwIfNotFound: true);
        m_gameplay_grabLeft = m_gameplay.FindAction("grabLeft", throwIfNotFound: true);
        m_gameplay_releaseLeft = m_gameplay.FindAction("releaseLeft", throwIfNotFound: true);
        m_gameplay_grabRight = m_gameplay.FindAction("grabRight", throwIfNotFound: true);
        m_gameplay_releaseRight = m_gameplay.FindAction("releaseRight", throwIfNotFound: true);
        m_gameplay_throw = m_gameplay.FindAction("throw", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // gameplay
    private readonly InputActionMap m_gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_gameplay_moveLeft;
    private readonly InputAction m_gameplay_moveRight;
    private readonly InputAction m_gameplay_grabLeft;
    private readonly InputAction m_gameplay_releaseLeft;
    private readonly InputAction m_gameplay_grabRight;
    private readonly InputAction m_gameplay_releaseRight;
    private readonly InputAction m_gameplay_throw;
    public struct GameplayActions
    {
        private @HandyInputs m_Wrapper;
        public GameplayActions(@HandyInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @moveLeft => m_Wrapper.m_gameplay_moveLeft;
        public InputAction @moveRight => m_Wrapper.m_gameplay_moveRight;
        public InputAction @grabLeft => m_Wrapper.m_gameplay_grabLeft;
        public InputAction @releaseLeft => m_Wrapper.m_gameplay_releaseLeft;
        public InputAction @grabRight => m_Wrapper.m_gameplay_grabRight;
        public InputAction @releaseRight => m_Wrapper.m_gameplay_releaseRight;
        public InputAction @throw => m_Wrapper.m_gameplay_throw;
        public InputActionMap Get() { return m_Wrapper.m_gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @moveLeft.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMoveLeft;
                @moveLeft.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMoveLeft;
                @moveLeft.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMoveLeft;
                @moveRight.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMoveRight;
                @moveRight.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMoveRight;
                @moveRight.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMoveRight;
                @grabLeft.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGrabLeft;
                @grabLeft.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGrabLeft;
                @grabLeft.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGrabLeft;
                @releaseLeft.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReleaseLeft;
                @releaseLeft.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReleaseLeft;
                @releaseLeft.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReleaseLeft;
                @grabRight.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGrabRight;
                @grabRight.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGrabRight;
                @grabRight.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnGrabRight;
                @releaseRight.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReleaseRight;
                @releaseRight.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReleaseRight;
                @releaseRight.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReleaseRight;
                @throw.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnThrow;
                @throw.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnThrow;
                @throw.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnThrow;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @moveLeft.started += instance.OnMoveLeft;
                @moveLeft.performed += instance.OnMoveLeft;
                @moveLeft.canceled += instance.OnMoveLeft;
                @moveRight.started += instance.OnMoveRight;
                @moveRight.performed += instance.OnMoveRight;
                @moveRight.canceled += instance.OnMoveRight;
                @grabLeft.started += instance.OnGrabLeft;
                @grabLeft.performed += instance.OnGrabLeft;
                @grabLeft.canceled += instance.OnGrabLeft;
                @releaseLeft.started += instance.OnReleaseLeft;
                @releaseLeft.performed += instance.OnReleaseLeft;
                @releaseLeft.canceled += instance.OnReleaseLeft;
                @grabRight.started += instance.OnGrabRight;
                @grabRight.performed += instance.OnGrabRight;
                @grabRight.canceled += instance.OnGrabRight;
                @releaseRight.started += instance.OnReleaseRight;
                @releaseRight.performed += instance.OnReleaseRight;
                @releaseRight.canceled += instance.OnReleaseRight;
                @throw.started += instance.OnThrow;
                @throw.performed += instance.OnThrow;
                @throw.canceled += instance.OnThrow;
            }
        }
    }
    public GameplayActions @gameplay => new GameplayActions(this);
    private int m_GamePadSchemeIndex = -1;
    public InputControlScheme GamePadScheme
    {
        get
        {
            if (m_GamePadSchemeIndex == -1) m_GamePadSchemeIndex = asset.FindControlSchemeIndex("GamePad");
            return asset.controlSchemes[m_GamePadSchemeIndex];
        }
    }
    public interface IGameplayActions
    {
        void OnMoveLeft(InputAction.CallbackContext context);
        void OnMoveRight(InputAction.CallbackContext context);
        void OnGrabLeft(InputAction.CallbackContext context);
        void OnReleaseLeft(InputAction.CallbackContext context);
        void OnGrabRight(InputAction.CallbackContext context);
        void OnReleaseRight(InputAction.CallbackContext context);
        void OnThrow(InputAction.CallbackContext context);
    }
}
