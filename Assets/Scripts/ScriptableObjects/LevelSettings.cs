﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSettings", menuName = "ScriptableObjects/LevelSettings")]
public class LevelSettings : ScriptableObject
{
    [Header("Hazards")]
    public Vector2 SpawArea = Vector2.one * 9;
    [Space]
    public float MaxCracks = 3;
    public float CracksInterval = 3;
    public float MinCrackInterval = 2;
    public float TimeToBreak = 1;
    public float TimeToFix = 1;
    [Space]
    public float MaxStones = 3;
    public float StonesInterval = 3;
    public float StoneSpawnChance = 0.5f;
    public float MaxSpawnChance = 1f; 

    [Header("Game")]
    public float MaxWaterFill = 100;
}
