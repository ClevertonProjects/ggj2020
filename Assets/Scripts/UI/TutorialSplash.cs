﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSplash : MonoBehaviour
{
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        
        Time.timeScale = 0;

        yield return new WaitUntil(() => Input.GetButton("Fire1"));

        Time.timeScale = 1;

        Destroy(gameObject);
    }
}
