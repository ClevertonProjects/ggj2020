﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TopUi : MonoBehaviour
{
    [SerializeField] private LevelSettings levelSettings;
    [Space]
    [SerializeField] private Image leftWaterFill;
    [SerializeField] private Image rightWaterFill;
    [Space]
    [SerializeField] private TextMeshProUGUI textP1;
    [SerializeField] private TextMeshProUGUI textP2;

    private void Awake()
    {
        GameController.OnGameOverAction += OnGameOver;
        StartCoroutine(UpdateUI());
    }

    private void OnDestroy()
    {
        GameController.OnGameOverAction -= OnGameOver;
    }

    private IEnumerator UpdateUI ()
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();

        while (true)
        {
            leftWaterFill.fillAmount = GameController.leftWaterFill / levelSettings.MaxWaterFill * 0.4f;
            rightWaterFill.fillAmount = GameController.rightWaterFill / levelSettings.MaxWaterFill * 0.4f;

            textP1.text = (leftWaterFill.fillAmount * 250).ToString("0") + "%";
            textP2.text = (rightWaterFill.fillAmount * 250).ToString("0") + "%";

            yield return delay;
        }
    }

    private void OnGameOver(float p1Fill, float p2Fill)
    {
        StopAllCoroutines();
        enabled = false;
    }
}
