﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StonesManager : MonoBehaviour
{
    [SerializeField] private GameObject stonePrefab;
    public LevelSettings levelSettings;

    public float minSpawnX;
    public float maxSpawnX;
    public float spawnY;

    private float spawnChance;

    void Start()
    {
        spawnChance = levelSettings.StoneSpawnChance;
        StartCoroutine(StoneCreationRoutine());
    }

    public void UpdateSpawnChance (float rate)
    {
        spawnChance = Mathf.Lerp(levelSettings.StoneSpawnChance, levelSettings.MaxSpawnChance, rate);
    }

    private bool CanSpawnStone ()
    {
        if (Random.value < spawnChance)
        {
            return true;
        }

        return false;
    }

    private IEnumerator StoneCreationRoutine ()
    {
        WaitForSeconds delay = new WaitForSeconds(levelSettings.StonesInterval);

        while (true)
        {
            yield return delay;

            if (CanSpawnStone())
            {
                SpawnStone();
            }
        }
    }

    private void SpawnStone()
    {           
        int randSide = Random.value < 0.5f ? -1 : 1;
        float randX = Random.Range(minSpawnX, maxSpawnX) * randSide;

        Vector3 spawnPosition = new Vector3(randX, spawnY, 0f);

        GameObject spawnedStone = Instantiate(stonePrefab, spawnPosition, Quaternion.identity);
        Stone newStone = spawnedStone.GetComponent<Stone>();
        newStone.transform.parent = transform;        
    }

    public void ThrowStoneFromPlayer (int playerIndex)
    {
        int side = playerIndex == 1 ? 1 : -1;

        float randX = Random.Range(minSpawnX, maxSpawnX) * side;

        Vector3 spawnPosition = new Vector3(randX, spawnY, 0f);

        GameObject spawnedStone = Instantiate(stonePrefab, spawnPosition, Quaternion.identity);
        Stone newStone = spawnedStone.GetComponent<Stone>();
        newStone.transform.parent = transform;
    }
}
