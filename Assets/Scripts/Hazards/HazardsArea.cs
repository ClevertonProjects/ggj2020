﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardsArea : MonoBehaviour
{
    private int hazardsCount = 0;    

    public LevelSettings levelSettings;
    [SerializeField] private Hazard hazardPrefab;    

    private void Awake()
    {
        GameController.OnGameOverAction += OnGameOver;
    }

    private void OnDestroy()
    {
        GameController.OnGameOverAction -= OnGameOver;
    }

    public void StartWave ()
    {        
        StartCoroutine(HazardSpawnRoutine());        
    }

    private IEnumerator HazardSpawnRoutine()
    {
        while(enabled)
        {
            if (hazardsCount >= levelSettings.MaxCracks)
                yield return new WaitUntil(() => hazardsCount < levelSettings.MaxCracks);

            yield return new WaitForSeconds(levelSettings.CracksInterval);

            SpawnHazard();
        }
    }    

    private void SpawnHazard()
    {
        hazardsCount++;
        Vector3 localPosition = transform.localPosition;
        float randY = localPosition.y + Random.Range(-levelSettings.SpawArea.y, levelSettings.SpawArea.y);

        bool sidesOnly = randY <= 1.25f;
        int randSide = Random.value < .5f ? -1 : 1;

        float randX = localPosition.x + (sidesOnly ? randSide * Random.Range(1f, levelSettings.SpawArea.x) : 
                                        Random.Range(-levelSettings.SpawArea.x, levelSettings.SpawArea.x));

        Hazard newHazard = Instantiate(hazardPrefab, new Vector2(randX, randY), Quaternion.identity);
        newHazard.transform.parent = transform;
        newHazard.Initialize(OnHoleFixed);
    }   

    private void OnHoleFixed ()
    {
        hazardsCount--;
    }

    private void OnGameOver(float p1Fill, float p2Fill)
    {
        enabled = false;
        StopAllCoroutines();
    }    
}
