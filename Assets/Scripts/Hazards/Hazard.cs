﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hazard : MonoBehaviour
{
    public enum HazardStatus { Crack, Broken }
    public HazardStatus status = HazardStatus.Crack;

    [SerializeField] private LevelSettings levelSettings;
    [SerializeField] private GameObject streamParticles;
    [SerializeField] private Image radialFill;

    private float timeToFix = 1;

    private bool stopPoints;
    private const string BreakTriggerTag = "Open";

    public delegate void OnFixed();
    private OnFixed onFixed;

    [Header("Audio")]
    [SerializeField] private AudioSource crackSfx;
    [SerializeField] private AudioSource openSfx;
    [SerializeField] private AudioSource fixedSfx;
    [SerializeField] private AudioSource fillSfx;
    [SerializeField] private AudioSource streamSfx;

    private HandTrigger handTrigger;
    private HandsController handController;

    public void Initialize (OnFixed onFixedCallback)
    {
        onFixed = onFixedCallback;
        timeToFix = levelSettings.TimeToFix;

        GameController.OnGameOverAction += OnGameOver;
    }

    private void OnDestroy()
    {        
        GameController.OnGameOverAction -= OnGameOver;
    }

    private void StopParticles()
    {
        ParticleSystem particles = streamParticles.GetComponentInChildren<ParticleSystem>();

        ParticleSystem.MainModule main = particles.main;
        main.stopAction = ParticleSystemStopAction.Destroy;
        main.loop = false;

        particles.Stop();
    }

    private int SetLevelProgressionTier (float waterLevel)
    {
        int result = 1;

        if (waterLevel > LevelProgression.ThirdTier)
        {
            result = 4;
        }
        else if (waterLevel > LevelProgression.SecondTier)
        {
            result = 3;
        }
        else if (waterLevel > LevelProgression.FirstTier)
        {
            result = 2;
        }

        return result;
    }

    private IEnumerator Start()
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        int actualTier = 0;

        if (crackSfx)
            crackSfx.Play();

        if (openSfx)
            openSfx.PlayDelayed(1.25f);

        yield return new WaitForSeconds(levelSettings.TimeToBreak);

        status = HazardStatus.Broken;
        GetComponent<Animator>().SetTrigger(BreakTriggerTag);
        streamParticles.SetActive(true);

        if(streamSfx)
            streamSfx.Play();

        while(!stopPoints)
        {
            if (transform.position.x < 0)
                GameController.leftWaterFill += Time.deltaTime;

            else if (transform.position.x > 0)
                GameController.rightWaterFill += Time.deltaTime;


            yield return delay;

            float heighterValue = GameController.leftWaterFill > GameController.rightWaterFill ? GameController.leftWaterFill : GameController.rightWaterFill;
            int tierLevel = SetLevelProgressionTier(heighterValue * 0.4f);
            
            if (tierLevel > actualTier)
            {
                actualTier = tierLevel;
                GameObject.FindGameObjectWithTag(Tags.StoneManager).GetComponent<StonesManager>().UpdateSpawnChance(actualTier/LevelProgression.MaxTiers);
            }

        }
    }

    private void FixHole ()
    {
        onFixed();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
            return;
        
        handController = collision.GetComponentInParent<HandsController>();
        handTrigger = collision.GetComponent<HandTrigger>();

        if (handController != null && handTrigger != null)
        {
            if ((handTrigger.IsRightHand && !handController.IsRightHandClosed) || (!handTrigger.IsRightHand && !handController.IsLeftHandClosed))
            {
                timeToFix -= Time.deltaTime;
                radialFill.fillAmount = 1 - (timeToFix / levelSettings.TimeToFix);

                if (timeToFix <= 0)
                {
                    stopPoints = true;
                    FixHole();
                    StopParticles();

                    GetComponent<Collider2D>().enabled = false;

                    radialFill.gameObject.SetActive(false);

                    GetComponent<Animator>().SetTrigger("Fixed");
                    fixedSfx.Play();

                    Destroy(gameObject, 1);

                    Destroy(crackSfx);
                    Destroy(openSfx);
                    Destroy(streamSfx);
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
            return;

        radialFill.enabled = true;

        fillSfx.Play();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
            return;

        radialFill.enabled = false;

        fillSfx.Pause();
    }

    private void OnGameOver(float p1Fill, float p2Fill)
    {
        enabled = false;
    }
}
