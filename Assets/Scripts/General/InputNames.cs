﻿public class InputNames
{
    public const string LHorizontal = "LeftHorizontal";                    //!< Left hand horizontal input.
    public const string RHorizontal = "RightHorizontal";                   //!< Right hand horizontal input.
    public const string LVertical = "LeftVertical";                         //!< Left hand vertical input.
    public const string RVertical = "RightVertical";                       //!< Right hand vertical input.
    public const string Fire = "Fire";                                     //!< Fire input.
    public const string GrabL = "GrabLeft";
    public const string GrabR = "GrabRight";
}
