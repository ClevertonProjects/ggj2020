using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*! FadeState enum, Lists the fade types.*/
public enum FadeState
{
    FadeIn,
    FadeOut
};

/*! \class SceneFader
 *  \brief Controls the scene transition fade effect.
 */
public class SceneFader : MonoBehaviour
{	
	
	[SerializeField]private float fadeSpeed;				        //!< Time speed of the fade.	    
    [SerializeField]private Image fadeImage;                        //!< Image of fade effect.
    private FadeState fadeState;    	                            //!< Check if the image needs to fade in or out.    

    public AnimationCurve fadeCurve;                                //!< Curve determining the fade time behavior.  

    // Callback for fade end.
    public delegate void Callback();
    Callback faderCallback;   
    
	/// Interpolates the color of the image to fade it in or out.
	private IEnumerator FadeTransition (float from, float to, float duration) 
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();        
        float elapsedTime = 0f;
        float percentageComplete = 0f;

        while (percentageComplete < 1f)
        {
            Color actualColor = fadeImage.color;
            actualColor.a = Mathf.Lerp(from, to, fadeCurve.Evaluate(percentageComplete));
            fadeImage.color = actualColor;

            yield return delay;

            elapsedTime += Time.deltaTime;
            percentageComplete = elapsedTime / duration;            
        }
       
        if (fadeState == FadeState.FadeOut)
        {
            fadeImage.enabled = false;
        }        

        if (faderCallback != null)
        {
            faderCallback();
        }       
    }

	/// <summary>
	/// Starts to fade the scene accordingly to the option selected (in or out).
	/// </summary>
	public void FadeScene (FadeState fadeOption, Callback callback = null)
    {        
        fadeState = fadeOption;        
        faderCallback = callback;
        float from = 0f;
        float to = 0f;
        
        if (fadeState == FadeState.FadeOut)
        {
            from = 1f;
            to = 0f;
        }
        else if (fadeState == FadeState.FadeIn)
        {
            from = 0f;
            to = 1f;
        }

        StartCoroutine(FadeTransition(from, to, fadeSpeed));
	}

    /// <summary>
    /// Sets a specific color to the fade image.
    /// </summary>
    public void SetFadeColor (Color newColor)
    {
        fadeImage.color = newColor;
    }
}
