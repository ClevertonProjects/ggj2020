﻿public class LevelProgression
{
    public const float FirstTier = 30;
    public const float SecondTier = 60;
    public const float ThirdTier = 88;

    public const float MaxTiers = 4;
}
