﻿using System.Collections;
using UnityEngine;

public class StoneThrowPath : MonoBehaviour
{
    [SerializeField] private Transform myTransform;
    [SerializeField] private float moveDuration;
    [SerializeField] private AnimationCurve moveBehavior;

    [SerializeField] private Vector3 toPosition;
    [SerializeField] private Vector3 initialLeftPos1;
    [SerializeField] private Vector3 initialRightPos1;    

    public void SetInitialPosition (bool isRightHand)
    {
        if (isRightHand)
        {            
            myTransform.position = initialRightPos1;            
        }
        else
        {            
            myTransform.position = initialLeftPos1;            
        }
    }

    public void StartMovement ()
    {        
        gameObject.SetActive(true);
        StartCoroutine(MovingRoutine(myTransform.position));
    }

    private IEnumerator MovingRoutine (Vector3 fromPos)
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float elapsedTime = 0f;
        float progression = 0f;

        while (progression < 1f)
        {
            myTransform.position = Vector3.Lerp(fromPos, toPosition, moveBehavior.Evaluate(progression));

            yield return delay;

            elapsedTime += Time.deltaTime;
            progression = elapsedTime / moveDuration;            
        }

        gameObject.SetActive(false);
    }
}
