﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public class HandsController : MonoBehaviour
{
    [SerializeField] private byte joystick = 1;                             //!< Which joystick to listen to
    [SerializeField] private float moveSpeed;                               //!< Hand moving speed.
    [SerializeField] private float maxRadius;                               //!< Maximum distance radius to move.
    [SerializeField] private Transform leftHand;                            //!< Left hand Transform.
    [SerializeField] private Transform rightHand;                           //!< Right hand Transform.
    
    [SerializeField] private Sprite openHandSprite;
    [SerializeField] private Sprite closedHandSprite;    

    [SerializeField] private SpriteRenderer leftSprite;
    [SerializeField] private SpriteRenderer rightSprite;

    [SerializeField] private HandTrigger leftHandTrigger;
    [SerializeField] private HandTrigger rightHandTrigger;

    [SerializeField] private float hitBlockDuration;

    [SerializeField] private Animator anim;

    [SerializeField] private StoneThrowPath stonePath;

    [Header("Audio")]
    [SerializeField] private AudioSource[] ouchSfxs;
    [SerializeField] private AudioSource throwSfx;

    private HandyInputs inputActions;

    private bool isLeftHandClosed;
    private bool isRightHandClosed;

    private bool triggerLeftHandClose;
    private bool triggerRightHandClose;

    private Transform myTransform;                                          //!< Character's transform.

    private bool isStunned;                                                 //!< 

    private const string ThrowLeftTag = "ThrowLeft";
    private const string ThrowRightTag = "ThrowRight";

    private Vector2 leftStickInput;
    private Vector2 rightStickInput;

    public bool IsLeftHandClosed
    {
        get { return isLeftHandClosed; }
    }

    public bool IsRightHandClosed
    {
        get { return isRightHandClosed; }
    }

    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }

    private void Awake()
    {
        inputActions = new HandyInputs();                
        InputUser _user = InputUser.PerformPairingWithDevice(Gamepad.all[joystick]);
        _user.AssociateActionsWithUser(inputActions);
        _user.ActivateControlScheme(inputActions.controlSchemes[0]);        

        inputActions.gameplay.grabLeft.performed += ctx => OnGrabLeftPress(ctx);
        inputActions.gameplay.grabRight.performed += ctx => OnGrabRightPress(ctx);
        inputActions.gameplay.releaseLeft.performed += ctx => OnGrabLeftRelease(ctx);
        inputActions.gameplay.releaseRight.performed += ctx => OnGrabRightRelease(ctx);
        inputActions.gameplay.@throw.performed += ctx => Throw(ctx);
    }    

    // Start is called before the first frame update
    void Start()
    {
        
        myTransform = transform;        
        GameController.OnGameOverAction += OnGameOver;    
    }

    private void OnDestroy()
    {
        GameController.OnGameOverAction -= OnGameOver;
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInputs();

        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>().TriggerSceneLoading();
        //}
    }

    private void ProcessInputs ()
    {
        if (!isStunned)
        {
            leftStickInput = inputActions.gameplay.moveLeft.ReadValue<Vector2>();
            rightStickInput = inputActions.gameplay.moveRight.ReadValue<Vector2>();
            //GetInputs();
            MoveHand(leftStickInput.x, leftStickInput.y, leftHand);
            MoveHand(rightStickInput.x, rightStickInput.y, rightHand);

            SetHandAngle(rightHand);
            SetHandAngle(leftHand);

            if (leftHandTrigger.HasStone && !triggerLeftHandClose)
            {
                leftHandTrigger.DropStone();
            }
            else if (rightHandTrigger.HasStone && !triggerRightHandClose)
            {
                rightHandTrigger.DropStone();
            }            
        }
    }

    private void SetHandAngle (Transform handTransform)
    {
        Vector3 handDirection = handTransform.position - myTransform.position;
        float angle = Mathf.Atan2(handDirection.y, handDirection.x) * Mathf.Rad2Deg;
        
        handTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void MoveHand (float horizontal, float vertical, Transform handTransform)
    {        
        Vector3 movingDir = Vector3.right * horizontal + Vector3.up * vertical;
        Vector3 newPosition = handTransform.position + movingDir * moveSpeed * Time.deltaTime;
        float distance = Vector3.Distance(newPosition, myTransform.position);

        if (distance > maxRadius)
        {
            Vector3 dir = newPosition - myTransform.position;
            dir *= maxRadius / distance;
            newPosition = myTransform.position + dir;
        }        

        handTransform.position = newPosition;
    }

    public void OnMoveLeft (InputAction.CallbackContext context)
    {
        leftStickInput = context.ReadValue<Vector2>();
    }

    public void OnMoveRight (InputAction.CallbackContext context)
    {
        rightStickInput = context.ReadValue<Vector2>();
    }

    public void OnGrabLeftPress (InputAction.CallbackContext context)
    {        
        triggerLeftHandClose = true;        
    }

    public void OnGrabLeftRelease (InputAction.CallbackContext context)
    {    
        triggerLeftHandClose = false;
    }

    public void OnGrabRightPress (InputAction.CallbackContext context)
    {        
        triggerRightHandClose = true;
    }

    public void OnGrabRightRelease (InputAction.CallbackContext context)
    {        
        triggerRightHandClose = false;
    }

    public void Throw (InputAction.CallbackContext context)
    {
        if ((leftHandTrigger.HasStone || rightHandTrigger.HasStone))
        {
            GameObject.FindGameObjectWithTag(Tags.StoneManager).GetComponent<StonesManager>().ThrowStoneFromPlayer(joystick);

            if (leftHandTrigger.HasStone)
            {
                stonePath.SetInitialPosition(false);
                anim.enabled = true;
                leftHandTrigger.ThrowStone();
                anim.SetTrigger(ThrowLeftTag);
                throwSfx.Play();
            }
            else
            {
                stonePath.SetInitialPosition(true);
                anim.enabled = true;
                rightHandTrigger.ThrowStone();
                anim.SetTrigger(ThrowRightTag);
                throwSfx.Play();
            }
        }
    }

    /// Gets the player inputs.
    private void GetInputs ()
    {
        float leftHandHor = Input.GetAxis(InputNames.LHorizontal + joystick);
        float leftHandVer = Input.GetAxis(InputNames.LVertical + joystick);
        float rightHandHor = Input.GetAxis(InputNames.RHorizontal + joystick);
        float rightHandVer = Input.GetAxis(InputNames.RVertical + joystick);

        MoveHand(leftHandHor, leftHandVer, leftHand);
        MoveHand(rightHandHor, rightHandVer, rightHand);

        SetHandAngle(rightHand);
        SetHandAngle(leftHand);

        triggerLeftHandClose = Input.GetButton(InputNames.GrabL + joystick);
        triggerRightHandClose = Input.GetButton(InputNames.GrabR + joystick);       
        
        if (leftHandTrigger.HasStone && !triggerLeftHandClose)
        {
            leftHandTrigger.DropStone();
        }
        else if (rightHandTrigger.HasStone && !triggerRightHandClose)
        {
            rightHandTrigger.DropStone();
        }

        if (Input.GetButtonDown(InputNames.Fire + joystick) && (leftHandTrigger.HasStone || rightHandTrigger.HasStone))
        {
            GameObject.FindGameObjectWithTag(Tags.StoneManager).GetComponent<StonesManager>().ThrowStoneFromPlayer(joystick);

            if (leftHandTrigger.HasStone)
            {
                stonePath.SetInitialPosition(false);
                anim.enabled = true;
                leftHandTrigger.ThrowStone();
                anim.SetTrigger(ThrowLeftTag);
                throwSfx.Play();
            }
            else
            {
                stonePath.SetInitialPosition(true);
                anim.enabled = true;
                rightHandTrigger.ThrowStone();
                anim.SetTrigger(ThrowRightTag);
                throwSfx.Play();
            }
        }
    }

    private void LateUpdate()
    {
        if (isStunned) return;

        if (triggerLeftHandClose && !isLeftHandClosed)
        {
            isLeftHandClosed = true;
            leftSprite.sprite = closedHandSprite;
            leftHandTrigger.GetCollistionInstantly();
        }
        else if (!triggerLeftHandClose && isLeftHandClosed)
        {
            isLeftHandClosed = false;
            leftSprite.sprite = openHandSprite;            
        }

        if (triggerRightHandClose && !isRightHandClosed)
        {
            isRightHandClosed = true;
            rightSprite.sprite = closedHandSprite;
            rightHandTrigger.GetCollistionInstantly();
        }
        else if (!triggerRightHandClose && isRightHandClosed)
        {
            isRightHandClosed = false;
            rightSprite.sprite = openHandSprite;
        }
    }

    private void OnGameOver(float p1Fill, float p2Fill)
    {
        enabled = false;
    }

    public void GetStunned()
    {
        if(!isStunned)
            ouchSfxs[Random.Range(0, ouchSfxs.Length)].Play();

        StartCoroutine(StunRoutine());
    }

    private IEnumerator StunRoutine()
    {
        anim.enabled = true;
        anim.SetBool("Hurt", true);
        isStunned = true;
        yield return new WaitForSeconds(hitBlockDuration);
        isStunned = false;
        anim.SetBool("Hurt", false);
        yield return new WaitForSeconds(0.3f);
        anim.enabled = false;
    }

    public void DisableAnimator ()
    {
        stonePath.StartMovement();
        anim.enabled = false;
    }
}
