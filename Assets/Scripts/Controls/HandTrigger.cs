﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandTrigger : MonoBehaviour
{
    [SerializeField] private HandsController handsController;
    [SerializeField] private bool isRightHand;

    private bool hasStone;
    private GameObject stoneObject;
    private Collider2D[] grabbedStones = new Collider2D[3];

    private const float ColliderRadius = 0.5f;

    public bool HasStone
    {
        get { return hasStone; }
    }

    public bool IsRightHand
    {
        get { return isRightHand; }
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.CompareTag(Tags.Stone))
        {            
            if (isRightHand && handsController.IsRightHandClosed || !isRightHand && handsController.IsLeftHandClosed)
            {
                hasStone = true;
                stoneObject = collision.gameObject;
                stoneObject.SetActive(false);
            }
        }
    }

    public void GetCollistionInstantly ()
    {
        int amount = Physics2D.OverlapCircleNonAlloc(transform.position, ColliderRadius, grabbedStones, 1 << 8);

        if (amount > 0)
        {
            hasStone = true;
            stoneObject = grabbedStones[0].gameObject;

            for (int i = 0; i < amount; i++)
            {
                grabbedStones[i].gameObject.SetActive(false);
            }
        }
    }

    public void ThrowStone ()
    {
        hasStone = false;
        Destroy(stoneObject);
    }

    public void DropStone ()
    {
        hasStone = false;
        if (stoneObject != null)
        {
            stoneObject.transform.position = transform.position;
            stoneObject.SetActive(true);
        }
    }
}
