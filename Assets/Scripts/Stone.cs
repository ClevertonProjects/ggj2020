﻿using UnityEngine;
using System.Collections;

public class Stone : MonoBehaviour
{
    [SerializeField] new private Rigidbody2D rigidbody2D;
    [SerializeField] private SpriteRenderer imgStone;
    [Header("Audio")]
    [SerializeField] private AudioSource hitSfx;           

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponentInParent<HandsController>().GetStunned();
            rigidbody2D.AddForce(collision.GetContact(0).normal * -10);
            StartCoroutine(Fade(2f));
        }

        hitSfx.Play();
    }    

    private IEnumerator Fade (float duration)
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float elapsedTime = 0f;
        float transitionRate = 0f;
        Color imgColor;

        while (transitionRate < 1f)
        {
            imgColor = imgStone.color;
            imgColor.a = Mathf.Lerp(1f, 0f, transitionRate);

            yield return delay;

            elapsedTime += Time.deltaTime;
            transitionRate = elapsedTime / duration;
        }

        Destroy(gameObject);
    }
}
