﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screenshots : MonoBehaviour
{
    public int supersize;

    static Screenshots instance;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    private void Update()
    {
        int shotNum = PlayerPrefs.GetInt("ShotNum", 0);
        if(Input.GetButtonDown("Fire3") || Input.GetKeyDown(KeyCode.Space))
        {
            ScreenCapture.CaptureScreenshot(string.Format("Shots/{0} Handy.png", shotNum.ToString("00")), supersize);
            PlayerPrefs.SetInt("ShotNum", shotNum + 1);
        }
    }
}
